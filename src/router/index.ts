import { createRouter, createWebHashHistory, createWebHistory, createMemoryHistory, RouteRecordRaw } from 'vue-router';

import Home from '../views/Home.vue';
import NotFound from '../views/NotFound.vue';
import { trackRouter } from "vue-gtag-next";

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/home',
    name: 'Home',
    meta: {
      title: 'Nike - Plataforma de treinamento',
      metaTags: [
        {
          name: 'description',
          content: 'The home page of our example app.'
        },
        {
          property: 'og:description',
          content: 'The home page of our example app.'
        }
      ]
    },
    component: () => import('../views/Home.vue')
  },
  {
    path: '/login/novaSenha/:token',
    name: 'newPassword',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/sobre',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  /* {
    path: '/feed',
    name: 'Feed',
    component: () => import('../views/Feed.vue'),
  },
  {
    path: '/my-feed',
    name: 'MyFeed',
    component: () => import('../views/MyFeed.vue'),
  }, */
  {
    path: '/colaborator',
    name: 'Colaborator',
    component: () => import('../views/Colaborator.vue'),
  },
  {
    path: '/share',
    name: 'Sharing',
    component: () => import('../views/Sharing.vue'),
  },
  {
    path: '/inicio',
    name: 'Inicio',
    meta: {
      title: 'Nike - Plataforma de treinamento',
      metaTags: [
        {
          name: 'description',
          content: 'The home page of our example app.'
        },
        {
          property: 'og:description',
          content: 'The home page of our example app.'
        }
      ]
    },
    component: () => import('../views/GeneralFeed.vue'),
  },
  {
    path: '/post/:slug',
    name: 'Post',
    component: () => import('../views/Single.vue')
  },
  {
    path: '/pesquisar',
    name: 'Search',
    component: () => import('../views/Search.vue')
  },
  {
    path: '/ajuda',
    name: 'Help',
    component: () => import('../views/Help.vue'),
    children: [{
      path: ':src/:id',
      name: 'Question',
      component: () => import('../views/Help.vue')
    }]
  },
  /* {
    path: '/ajuda/:src',
    name: 'Question',
    component: () => import('../views/Help.vue')
  }, */
  {
    path: '/perfil',
    name: 'Profile',
    component: () => import('../views/Profile.vue'),
  },
  { 
    path: '/perfil/reset/:token', 
    name: 'Reset',
    component: () => import('../views/Home.vue')
  },

  { 
    path: '/perfil/registro/:token', 
    name: 'CompletarRegistro',
    component: () => import('../views/Profile.vue')
  },
  { 
    path: '/styleguide', 
    name: 'Style',
    component: () => import('../views/StyleGuide.vue')
  },

  /* {
    path: '/perfil/reset/:token',
    name: 'Profile',
    component: () => import('../views/Profile.vue')
  }, */
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: NotFound

  },

]

const router = createRouter({
  history: createWebHistory(),
  routes
})
trackRouter(router);

export default router
