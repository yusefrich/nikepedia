import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './store'
import 'bootstrap'
import '../public/icons/css/style.css'
import apiService from './services/api'
import Maska from 'maska'
import { createHead } from '@vueuse/head'
import VueGtag from "vue-gtag-next";

apiService.init(process.env.VUE_APP_API_URL)
const head = createHead()
const gtag = {
    property: { id: "UA-199251907-1" }
}
createApp(App).use(store).use(router).use(Maska).use(head).use(VueGtag, gtag).mount('#app')
