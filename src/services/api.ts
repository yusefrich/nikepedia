import axios from 'axios';
import { ref } from "vue";

import { useStore, MutationTypes, ActionTypes } from '@/store';
import Swal from "sweetalert2";
import router from "@/router";
import authService from './modules/authService';

// eslint-disable-next-line no-use-before-define
/* import store from '@/store/index' */
const baseUrl = process.env.VUE_APP_API_URL;
const store = useStore();
const appdata = ref(store.state);


const apiService = {
    init(baseURL: string) {
        axios.defaults.baseURL = baseURL;
        axios.interceptors.request.use(
            (config) => {
        
                const token = localStorage.getItem('token')
        
                if (token) {
                    config.headers.common['Authorization'] = token
                }
                
                return config
            },
        
            (error) => Promise.reject(error)
        );
        
        axios.interceptors.response.use(
            res => {
                if (res.status == 401){
                    /* log out the user */
                    authService.logOut();
                }
                const hasAuthProperty = Object.prototype.hasOwnProperty.call(res.data, "token")
        
                if (hasAuthProperty ){
                    localStorage.setItem('token', "bearer "+res.data.token);
                }
                
                return res
            },
            error => {
              if (error.response.status == 401){
                /* store.dispatch('auth/logout'); */
              }

              return Promise.reject(error)
            }
        );
        const token = localStorage.getItem('token')
        
        if (token) {
            authService.auth();
        }
    },
    get(resource: string) {
        return axios.get(resource)
    }
}

export default apiService
