import { useStore, MutationTypes, ActionTypes } from '@/store';
import axios from 'axios';
import apiService from './api';


const baseUrl = process.env.VUE_APP_IBGE;
const store = useStore();
/* import { Hero } from './hero'; */

class IbgeService {

    /* constructor() {
    } */

    async getCountryStates(): Promise<void> {
        await axios.get(`${baseUrl}/localidades/estados`).then(response => {
            if (response.status == 200) {
                const data = response.data;
                store.dispatch(ActionTypes.FETCH_COUNTRY_STATE, data);
            }
        });
    }
    async getCityByState(state: string): Promise<void> {
        await axios.get(`${baseUrl}/localidades/estados/${state}/distritos`).then(response => {
            if (response.status == 200) {
                const data = response.data;
                store.dispatch(ActionTypes.FETCH_CITY, data);
            }
        });
    }
}

// Export a singleton instance in the global namespace
export default new IbgeService();
