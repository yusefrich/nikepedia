import { ref } from "vue";

import { useStore, MutationTypes, ActionTypes } from '@/store';

const store = useStore();
const appdata = ref(store.state);

const tipsService = {
    /* //* type can be alert, warning, success, info */
    async hideTips(id: string){
        let currentTips = "";
        if(localStorage.getItem('tips')){
            currentTips = localStorage.getItem('tips')+"";
        }
        currentTips = currentTips + id + ",";

        localStorage.setItem('tips', currentTips);
        return;
    },
    async showTips(){
        localStorage.setItem('tips', '');
        return;
    },
    getTipsStatus(id: string){
        const status = localStorage.getItem('tips')
        let statusReturn = true;
        let statusArray: any[] = []
        if(status){
            statusArray = status.split(",");
        }
        /* console.log("gettingTipsStatus from "+id)
        console.log(statusArray) */
        statusArray.forEach(e => {
            if(e == id){
                statusReturn = false;
            }
        });
        
        /* console.log(statusReturn)
        console.log("<--") */
        return statusReturn;
    }
}

export default tipsService
