import axios from 'axios';
import { ref } from "vue";

import { useStore, MutationTypes, ActionTypes } from '@/store';
import Swal from "sweetalert2";
import router from "@/router";

const baseUrl = process.env.VUE_APP_API_URL;
const store = useStore();
const appdata = ref(store.state);

const settingsService = {
    async settings(sucessF: any = null, errorF: any = null){
        let sucess = false;

        await axios.get(`${baseUrl}/settings`).then(res => {
            sucess = true;
            store.dispatch(ActionTypes.FETCH_SETTINGS, res.data.data);
            /* this.avatars(); */
            sucessF();
            return res;
        }).catch(error => {
            if(!sucess){
                if(errorF)
                    errorF("Erro no auth");
            }
            return error;
        });
        return;
    },
    async settingsHelp(sucessF: any = null, errorF: any = null){
        let sucess = false;

        await axios.get(`${baseUrl}/settings/help`).then(res => {
            sucess = true;
            store.dispatch(ActionTypes.FETCH_SETTINGS_HELP, res.data.data);
            /* this.avatars(); */
            sucessF();
            return res;
        }).catch(error => {
            if(!sucess){
                if(errorF)
                    errorF("Erro no auth");
            }
            return error;
        });
        return;
    },
}

export default settingsService
