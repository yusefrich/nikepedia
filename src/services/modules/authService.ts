import axios from 'axios';
import { ref } from "vue";

import { useStore, MutationTypes, ActionTypes } from '@/store';
import Swal from "sweetalert2";
import router from "@/router";

const baseUrl = process.env.VUE_APP_API_URL;
const store = useStore();
const appdata = ref(store.state);

const authService = {
    async auth(sucessF: any = null, errorF: any = null) {
        let sucess = false;

        await axios.post(`${baseUrl}/auth/me`).then(res => {
            sucess = true;
            store.dispatch(ActionTypes.FETCH_USER, res.data);
            this.avatars();
            sucessF();
            return res;
        }).catch(error => {
            if (!sucess) {
                if (errorF)
                    errorF("Erro no auth");
                localStorage.setItem('token', "");
                router.push('/')

            }
            return error;
        });
        return;
    },
    async authWithToken(token: any, sucessF: any = null, errorF: any = null) {
        let sucess = false;
        localStorage.setItem('token', "bearer " + token);

        await axios.post(`${baseUrl}/auth/me`).then(res => {
            sucess = true;
            store.dispatch(ActionTypes.FETCH_USER, res.data);
            this.avatars();
            sucessF();
            return res;
        }).catch(error => {
            if (!sucess) {
                if (errorF)
                    errorF("Erro no auth");
                localStorage.setItem('token', "");
                router.push('home')

            }
            return error;
        });
        return;
    },
    async login(resource: any, sucessF: any = null, errorF: any = null) {
        let sucess = false;
        await axios.post(`${baseUrl}/login/auth`, resource).then(res => {
            sucess = true;
            sucessF();
            console.log(res);

        }).catch(error => {
            if (!sucess) {
                
                errorF(error);
            }
        });

    },
    logOut() {
        store.dispatch(ActionTypes.FETCH_USER, {});
        localStorage.setItem('token', "");

    },
    async avatars(sucessF: any = null, errorF: any = null) {
        let sucess = false;
        await axios.get(`${baseUrl}/avatars`).then(res => {
            sucess = true;
            store.dispatch(ActionTypes.FETCH_AVATARS, res.data.data);

            if (sucessF)
                sucessF();
            console.log(res);

        }).catch(error => {
            if (!sucess) {
                if (errorF)
                    errorF("Erro no login");
            }
        });

    },

    async register(resource: any, sucessF: any = null, errorF: any = null) {
        console.log("line being called 1")
        let sucess = false;
        await axios.post(`${baseUrl}/register`, resource).then(res => {
            console.log("line being called 2")
            console.log(res)
            sucess = true
            sucessF(res);
            return res
        }).catch(error => {
            if (!sucess) {
                errorF(error);
            }
            return error
        });
    },
    async update(resource: any, sucessF: any = null, errorF: any = null) {
        console.log("line being called 1")
        let sucess = false;
        await axios.put(`${baseUrl}/users/update`, resource).then(res => {
            console.log("line being called 2")
            console.log(res)
            sucess = true
            sucessF();
            return res
        }).catch(error => {
            if (!sucess) {
                console.log("line being called 3")
                console.log(error.response)
                errorF("Erro na atualização do seu perfil, tente novamente!");
            }
            return error
        });
    },
    async registerPassword(resource: any, token: any, sucessF: any = null, errorF: any = null) {
        let sucess = false;
        await axios.put(`${baseUrl}/users/password/${token}`, resource).then(res => {
            console.log("senha criada com sucesso")
            console.log(res)
            sucess = true;
            if (sucessF)
                sucessF();
            return res
        }).catch(error => {
            if (!sucess) {
                console.log(error.response)
                if (errorF)
                    errorF("Erro no cadastro");
            }
            return error
        });
    },
    async forgotPassword(resource: any, sucessF: any = null, errorF: any = null) {
        let sucess = false;
        await axios.post(`${baseUrl}/forgot-password`, resource).then(res => {
            sucess = true;
            if (sucessF)
                sucessF();
            return res
        }).catch(error => {
            if (!sucess) {
                console.log(error.response)
                if (errorF)
                    errorF("Erro no cadastro");
            }
            return error
        });
    },
    async resetPassword(resource: any, sucessF: any = null, errorF: any = null) {
        let sucess = false;
        await axios.post(`${baseUrl}/reset-password`, resource).then(res => {
            sucess = true;
            if (sucessF)
                sucessF();
            return res
        }).catch(error => {
            if (!sucess) {
                console.log(error.response)
                if (errorF)
                    errorF("Erro no reset de senha");
            }
            return error
        });
    },
    async updatePassword(resource: any, sucessF: any = null, errorF: any = null) {
        let sucess = false;

        resource.token = localStorage.getItem('token');
        resource.email = appdata.value.user.email;
        await axios.post(`${baseUrl}/reset-password`, resource).then(res => {
            sucess = true;
            if (sucessF)
                sucessF();
            return res
        }).catch(error => {
            if (!sucess) {
                console.log(error.response)
                if (errorF)
                    errorF("Erro no cadastro");
            }
            return error
        });
    },
    async updateAvatar(resource: any, sucessF: any = null, errorF: any = null) {
        let sucess = false;
        await axios.put(`${baseUrl}/users/avatar`, resource).then(res => {
            sucess = true;
            if (sucessF)
                sucessF();
            return res
        }).catch(error => {
            if (!sucess) {
                console.log(error.response)
                if (errorF)
                    errorF("Erro no update de avatar");
            }
            return error
        });
    },
    async uploadAvatar(resource: any, sucessF: any = null, errorF: any = null) {
        let sucess = false;
        await axios.put(`${baseUrl}/users/avatar`, resource, {
            headers: {
                'Content-Type': `multipart/form-data; boundary=${resource._boundary}`
            }
        }).then(res => {
            sucess = true;
            if (sucessF)
                sucessF();
            return res
        }).catch(error => {
            if (!sucess) {
                console.log(error.response)
                if (errorF)
                    errorF("Erro no update de avatar");
            }
            return error
        });
    },
    async getFacebookRegisterLink(sucessF: any = null, errorF: any = null): Promise<string> {
        let sucess = false;
        let url = "";

        await axios.get(`${baseUrl}/login/facebook`).then(res => {
            sucess = true;
            if (sucessF)
                sucessF();
            console.log(res);
            url = res.data;

        }).catch(error => {
            if (!sucess) {
                if (errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
        return url;

    },
    async getGoogleRegisterLink(sucessF: any = null, errorF: any = null): Promise<string> {
        let sucess = false;
        let url = "";
        await axios.get(`${baseUrl}/login/google`).then(res => {
            sucess = true;
            if (sucessF)
                sucessF();
            console.log(res);
            url = res.data;

        }).catch(error => {
            if (!sucess) {
                if (errorF)
                    errorF("Erro ao buscar url do facebook, tente novamente");
            }
        });

        return url;
    },
    getUserImage(id: any): string {

        let src = "";
        if(!id){
          src = "/img/default.png";
        }
        appdata.value.avatars.forEach(e => {
          if(e.id == id){
            src = e.url;
          }
        });
        return src;
    },

}

export default authService
