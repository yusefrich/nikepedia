import axios from 'axios';
import { ref } from "vue";

import { useStore, MutationTypes, ActionTypes } from '@/store';
import Swal from "sweetalert2";
import router from "@/router";

const baseUrl = process.env.VUE_APP_API_URL;
const store = useStore();
const appdata = ref(store.state);

const sacService = {
    async helpRequest(payload: any, sucessF: any = null, errorF: any = null): Promise<void>{
        let sucess = false;
        await axios.post(`${baseUrl}/contacts`, payload).then(res => {
            sucess = true;
            if(sucessF)
                sucessF();
            console.log(res);

        }).catch(error => {
            if(!sucess){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },

}

export default sacService
