import axios from 'axios';
import { ref } from "vue";

import { useStore, MutationTypes, ActionTypes } from '@/store';
import Swal from "sweetalert2";
import router from "@/router";

const baseUrl = process.env.VUE_APP_API_URL;
const store = useStore();
const appdata = ref(store.state);

const locationService = {
    async getUf( sucessF: any = null, errorF: any = null): Promise<void>{
        let sucess = false;
        await axios.get(`${baseUrl}/locals/ufs`).then(res => {
            sucess = true;
            res.data.data.sort(function(a: any, b: any){
                if(a.uf < b.uf) { return -1; }
                if(a.uf > b.uf) { return 1; }
                return 0;
            })
            
            store.dispatch(ActionTypes.FETCH_COUNTRY_STATE, res.data.data);
            if(sucessF)
                sucessF();
            console.log(res);

        }).catch(error => {
            if(!sucess){
                if(errorF)
                    errorF("Erro no retorno de estados");
            }
        });
    },
    async getCities(uf: any, sucessF: any = null, errorF: any = null): Promise<void>{
        let sucess = false;
        await axios.get(`${baseUrl}/locals/${uf}/cities`).then(res => {
            sucess = true;
            res.data.data.sort(function(a: any, b: any){
                if(a.city < b.city) { return -1; }
                if(a.city > b.city) { return 1; }
                return 0;
            })
            store.dispatch(ActionTypes.FETCH_CITY, res.data.data);
            if(sucessF)
                sucessF();
            console.log(res);

        }).catch(error => {
            if(!sucess){
                if(errorF)
                    errorF("Erro no retorno das cidades");
            }
        });
    },
    async getLocals(city: any, sucessF: any = null, errorF: any = null): Promise<void>{
        let sucess = false;
        await axios.get(`${baseUrl}/locals/${city}`).then(res => {
            sucess = true;
            res.data.data.sort(function(a: any, b: any){
                if(a.name < b.name) { return -1; }
                if(a.name > b.name) { return 1; }
                return 0;
            })

            store.dispatch(ActionTypes.FETCH_LOCALS, res.data.data);
            if(sucessF)
                sucessF();
            console.log(res);

        }).catch(error => {
            if(!sucess){
                if(errorF)
                    errorF("Erro no retorno das lojas");
            }
        });
    },
    async getStores(local: any, sucessF: any = null, errorF: any = null): Promise<void>{
        let sucess = false;
        await axios.get(`${baseUrl}/locals/${local}/stores`).then(res => {
            sucess = true;
            res.data.data.sort(function(a: any, b: any){
                if(a.name < b.name) { return -1; }
                if(a.name > b.name) { return 1; }
                return 0;
            })


            store.dispatch(ActionTypes.FETCH_STORES, res.data.data);
            if(sucessF)
                sucessF();
            console.log(res);

        }).catch(error => {
            if(!sucess){
                if(errorF)
                    errorF("Erro no retorno das lojas");
            }
        });
    },

}

export default locationService
