import axios from 'axios';
import { ref } from "vue";

import { useStore, ActionTypes } from '@/store';

const baseUrl = process.env.VUE_APP_API_URL;
const store = useStore();
const appdata = ref(store.state);

const postsService = {
    async setFilters( filters: {content: any; category: any; subcategory: any; search: any; sort: any; station: any} , successF: any = null, errorF: any = null): Promise<void>{
        // eslint-disable-next-line
        store.dispatch(ActionTypes.FETCH_FILTERS, filters);
        return;
    },
    async getPosts(successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.get(`${baseUrl}/posts?content=${appdata.value.filters.content}&category=${appdata.value.filters.category}&subcategory=${appdata.value.filters.subcategory}&search=${appdata.value.filters.search}&sort=${appdata.value.filters.sort}&station=${appdata.value.filters.station}`).then(res => {
            success = true;
            store.dispatch(ActionTypes.FETCH_POSTS, res.data.data);
            if(successF)
                successF();
        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    setSharedPost(slug: string) {
        store.dispatch(ActionTypes.FETCH_SHARED_POST, slug);
    },
    unsetSharedPost() {
        store.dispatch(ActionTypes.FETCH_SHARED_POST, '');
    },
    async getPostsFromBothCategories(successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.get(`${baseUrl}/posts?content=""&category=${appdata.value.filters.category}&subcategory=${appdata.value.filters.subcategory}&search=${appdata.value.filters.search}&sort=${appdata.value.filters.sort}&station=${appdata.value.filters.station}`).then(res => {
            success = true;
            store.dispatch(ActionTypes.FETCH_POSTS, res.data.data);
            if(successF)
                successF();
        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async showPost(slug: string, successF: any = null, errorF: any = null): Promise<any>{
        let success = false;
        let post = {};

        await axios.get(`${baseUrl}/posts/${slug}`).then(res => {
            success = true;
            post = res.data.data;
            if(successF)
                successF();

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro na listagem do post");
            }
        });

        return post;
    },
    async getStations(successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.get(`${baseUrl}/posts/stations`).then(res => {
            success = true;
            store.dispatch(ActionTypes.FETCH_STATIONS, res.data.data);
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os estações");
            }
        });
    },
    async getFeatured(successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.get(`${baseUrl}/posts/featured?content=${appdata.value.filters.content}&category=${appdata.value.filters.category}&subcategory=${appdata.value.filters.subcategory}&search=${appdata.value.filters.search}&sort=${appdata.value.filters.sort}&station=${appdata.value.filters.station}`).then(res => {
            success = true;
            store.dispatch(ActionTypes.FETCH_FEATURED, res.data.data);
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async likePost(postId: number, successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.post(`${baseUrl}/posts/${postId}/likes`, {}).then(res => {
            success = true;
            /* this.getPosts(); */
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async dislikePost(postId: number, successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.delete(`${baseUrl}/posts/${postId}/likes/0`, {}).then(res => {
            success = true;
            /* this.getPosts(); */
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async getLikes(postId: number, successF: any = null, errorF: any = null): Promise<[]>{
        let success = false;
        let likes: [] = [];
        await axios.get(`${baseUrl}/posts/${postId}/likes`).then(res => {
            success = true;
            if(successF)
                successF();
            /* console.log(res); */
            likes = res.data.data;


        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
        return likes;
    },

    async sharePost(postId: number, successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.post(`${baseUrl}/posts/${postId}/shares`, {}).then(res => {
            success = true;
            /* this.getPosts(); */
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async favPost(postId: number, successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.post(`${baseUrl}/posts/${postId}/favorites`).then(res => {
            success = true;
            /* this.getPosts(); */
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async unfavPost(postId: number, successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.delete(`${baseUrl}/posts/${postId}/favorites/0`).then(res => {
            success = true;
            /* this.getPosts(); */
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async getFavs(postId: number, successF: any = null, errorF: any = null): Promise<[]>{
        let success = false;
        let favs: [] = [];
        await axios.get(`${baseUrl}/posts/${postId}/favorites`).then(res => {
            success = true;
            if(successF)
                successF();
            /* console.log(res); */
            favs = res.data.data;


        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
        return favs;
    },

    async starsPost(postId: number, stars: number, successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.post(`${baseUrl}/posts/${postId}/stars`, {stars}).then(res => {
            success = true;
            /* this.getPosts(); */
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async getStars(postId: number, successF: any = null, errorF: any = null): Promise<[]>{
        let success = false;
        let stars: [] = [];
        await axios.get(`${baseUrl}/posts/${postId}/stars`).then(res => {
            success = true;
            if(successF)
                successF();
            /* console.log(res); */
            stars = res.data.data;


        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
        return stars;
    },
    resetCats(){
        store.dispatch(ActionTypes.FETCH_CATEGORIES, []);
    },
    async getCats(successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.get(`${baseUrl}/posts/categories?content=${appdata.value.filters.content}`).then(res => {
            success = true;
            store.dispatch(ActionTypes.FETCH_CATEGORIES, res.data.data);
            if(successF)
                successF();
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
    },
    async commentPost(payload: any, postId: number, successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.post(`${baseUrl}/posts/${postId}/comments`, payload).then(res => {
            success = true;
            /* this.getPosts(); */
            console.log(res);
            if(res.data.errors){
                if(errorF && res.data.errors.comments)
                    errorF(res.data.errors.comments);
            }
            else if(successF){
                successF();
            }
            /* console.log(res); */

        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao comentar em um post");
            }
        });
    },
    async getComments(postId: number, successF: any = null, errorF: any = null): Promise<[]>{
        let success = false;
        let comments: [] = [];
        await axios.get(`${baseUrl}/posts/${postId}/comments`).then(res => {
            success = true;
            if(successF)
                successF();
            /* console.log(res); */
            comments = res.data.data;
        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
        return comments;
    },
    async deleteComments(postId: number, commentId: number, successF: any = null, errorF: any = null): Promise<void>{
        let success = false;
        await axios.delete(`${baseUrl}/posts/${postId}/comments/${commentId}`).then(res => {
            success = true;
            if(successF)
                successF();
        }).catch(error => {
            if(!success){
                if(errorF)
                    errorF("Erro ao buscar os posts");
            }
        });
        return;

    }

}

export default postsService
