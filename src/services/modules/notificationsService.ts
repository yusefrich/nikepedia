import { ref } from "vue";

import { useStore, MutationTypes, ActionTypes } from '@/store';

const store = useStore();
const appdata = ref(store.state);

const notificationsService = {
    /* //* type can be danger, warning, success, info */
    async add(notification: {type: string; message: string}, sucessF: any = null, errorF: any = null){

        store.dispatch(ActionTypes.INC_NOTIFICATIONS, notification);

        return;
    },
    async remove(id: string, sucessF: any = null, errorF: any = null){

        store.dispatch(ActionTypes.REMOVE_NOTIFICATIONS, id);

        return;
    },
}

export default notificationsService
