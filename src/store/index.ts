import {
    createStore,
    MutationTree,
    ActionContext,
    ActionTree,
    GetterTree,
    Store as VuexStore,
    CommitOptions,
    DispatchOptions,
    createLogger
} from "vuex";

//declare state
export type State = {
    filters: {
        content: string;
        category: string;
        subcategory: string;
        search: string;
        sort: string;
        station: string;
    };
    counter: number;
    sharedPost: string;
    settings: any[];
    settingsHelp: any[];
    countryState: object;
    city: any[];
    locals: object;
    stores: any[];
    posts: object;
    featured: object;
    stations: object;
    avatars: any[];
    categories: object[];
    user: any;
    notifications: { id: string; type: string; message: string }[];
};

//set state
const state: State = {
    filters: {
        content: "",
        category: "",
        subcategory: "",
        search: "",
        sort: "",
        station: "",
    },
    counter: 0,
    sharedPost: '',
    settings: [],
    settingsHelp: [],
    countryState: {},
    city: [],
    locals: {},
    stores: [],
    posts: {},
    stations: {},
    featured: {},
    avatars: [],
    categories: [],
    /* eslint-disable */
    user: {
        avatar_id: null,
        birthday: null,
        content_id: null,
        created_at: null,
        email: null,
        email_verified_at: null,
        facebook_token: null,
        genre: null,
        google_token: null,
        id: null,
        last_name: null,
        name: null,
        profile: null,
        roles: [],
        status: null,
        store_id: null,
        updated_at: null,
    },
    /* eslint-enable */
    notifications: [],

};

// mutations and action enums

export enum MutationTypes {
    INC_COUNTER = "SET_COUNTER",
    SET_SHARED_POST = "SET_SHARED_POST",
    INC_NOTIFICATIONS = "SET_NOTIFICATIONS",
    REMOVE_NOTIFICATIONS = "REMOVE_NOTIFICATIONS",
    SET_FILTERS = "SET_FILTERS",
    SET_SETTINGS = "SET_SETTINGS",
    SET_SETTINGS_HELP = "SET_SETTINGS_HELP",
    SET_COUNTRY_STATE = "SET_COUNTRY_STATE",
    SET_CITY = "SET_CITY",
    SET_LOCALS = "SET_LOCALS",
    SET_STORES = "SET_STORES",
    SET_POSTS = "SET_POSTS",
    SET_STATIONS = "SET_STATIONS",
    SET_FEATURED = "SET_FEATURED",
    SET_AVATARS = "SET_AVATARS",
    SET_CATEGORIES = "SET_CATEGORIES",
    SET_USER = "SET_USER"
}

export enum ActionTypes {
    INC_COUNTER = "SET_COUNTER",
    FETCH_SHARED_POST = "FETCH_SHARED_POST",
    INC_NOTIFICATIONS = "SET_NOTIFICATIONS",
    REMOVE_NOTIFICATIONS = "REMOVE_NOTIFICATIONS",
    FETCH_FILTERS = "FETCH_FILTERS",
    FETCH_SETTINGS = "FETCH_SETTINGS",
    FETCH_SETTINGS_HELP = "FETCH_SETTINGS_HELP",
    FETCH_COUNTRY_STATE = "FETCH_COUNTRY_STATE",
    FETCH_CITY = "FETCH_CITY",
    FETCH_LOCALS = "FETCH_LOCALS",
    FETCH_STORES = "FETCH_STORES",
    FETCH_POSTS = "FETCH_POSTS",
    FETCH_STATIONS = "FETCH_STATIONS",
    FETCH_FEATURED = "FETCH_FEATURED",
    FETCH_AVATARS = "FETCH_AVATARS",
    FETCH_CATEGORIES = "FETCH_CATEGORIES",
    FETCH_USER = "FETCH_USER"
}

//Mutation Types
export type Mutations<S = State> = {
    [MutationTypes.INC_COUNTER](state: S, payload: number): void;
    [MutationTypes.INC_NOTIFICATIONS](state: S, payload: object): void;
    [MutationTypes.REMOVE_NOTIFICATIONS](state: S, id: string): void;
    [MutationTypes.SET_SETTINGS](state: S, payload: object): void;
    [MutationTypes.SET_SETTINGS_HELP](state: S, payload: object): void;
    [MutationTypes.SET_FILTERS](state: S, payload: object): void;
    [MutationTypes.SET_COUNTRY_STATE](state: S, payload: object): void;
    [MutationTypes.SET_CITY](state: S, payload: object): void;
    [MutationTypes.SET_LOCALS](state: S, payload: object): void;
    [MutationTypes.SET_STORES](state: S, payload: object): void;
    [MutationTypes.SET_POSTS](state: S, payload: object): void;
    [MutationTypes.SET_SHARED_POST](state: S, payload: string): void;
    [MutationTypes.SET_STATIONS](state: S, payload: object): void;
    [MutationTypes.SET_FEATURED](state: S, payload: object): void;
    [MutationTypes.SET_AVATARS](state: S, payload: object): void;
    [MutationTypes.SET_CATEGORIES](state: S, payload: object): void;
    [MutationTypes.SET_USER](state: S, payload: object): void;
};

//define mutations
const mutations: MutationTree<State> & Mutations = {
    [MutationTypes.INC_COUNTER](state: State, payload: number) {
        state.counter += payload;
    },
    [MutationTypes.INC_NOTIFICATIONS](state: State, payload: {type: string; message: string}) {
        state.notifications.push({
            ...payload,
            id: (Math.random().toString(36) + Date.now().toString(36)).substr(2)
        });
    },
    [MutationTypes.REMOVE_NOTIFICATIONS](state: State, id: string) {
        state.notifications = state.notifications.filter(notification => {
            return notification.id != id;
        })
    },
    [MutationTypes.SET_FILTERS](state: State, payload: { content: string; category: string; subcategory: string; search: string; sort: string; station: string }) {
        state.filters = payload;
    },
    [MutationTypes.SET_SETTINGS](state: State, payload: any[]) {
        state.settings = payload;
    },
    [MutationTypes.SET_SETTINGS_HELP](state: State, payload: any[]) {
        state.settingsHelp = payload;
    },
    [MutationTypes.SET_COUNTRY_STATE](state: State, payload: object) {
        state.countryState = payload;
    },
    [MutationTypes.SET_CITY](state: State, payload: any[]) {
        state.city = payload;
    },
    [MutationTypes.SET_LOCALS](state: State, payload: object) {
        state.locals = payload;
    },
    [MutationTypes.SET_STORES](state: State, payload: any[]) {
        state.stores = payload;
    },
    [MutationTypes.SET_POSTS](state: State, payload: object) {
        state.posts = payload;
    },
    [MutationTypes.SET_SHARED_POST](state: State, payload: string) {
        state.sharedPost = payload;
    },
    [MutationTypes.SET_STATIONS](state: State, payload: object) {
        state.stations = payload;
    },
    [MutationTypes.SET_FEATURED](state: State, payload: object) {
        state.featured = payload;
    },
    [MutationTypes.SET_AVATARS](state: State, payload: object[]) {
        state.avatars = payload;
    },
    [MutationTypes.SET_CATEGORIES](state: State, payload: object[]) {
        state.categories = payload;
    },
    [MutationTypes.SET_USER](state: State, payload: object) {
        state.user = payload;
    }
};

//actions

type AugmentedActionContext = {
    commit<K extends keyof Mutations>(
        key: K,
        payload: Parameters<Mutations[K]>[1]
    ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, State>, "commit">;

// actions interface

export interface Actions {
    [ActionTypes.INC_COUNTER](
        { commit }: AugmentedActionContext,
        payload: number
    ): void;
    [ActionTypes.REMOVE_NOTIFICATIONS](
        { commit }: AugmentedActionContext,
        id: string
    ): void;
    [ActionTypes.INC_NOTIFICATIONS](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_FILTERS](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_SETTINGS](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_SETTINGS_HELP](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_COUNTRY_STATE](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_CITY](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_LOCALS](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_STORES](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_POSTS](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_SHARED_POST](
        { commit }: AugmentedActionContext,
        payload: string
    ): void;
    [ActionTypes.FETCH_STATIONS](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_FEATURED](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_AVATARS](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_CATEGORIES](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
    [ActionTypes.FETCH_USER](
        { commit }: AugmentedActionContext,
        payload: object
    ): void;
}

export const actions: ActionTree<State, State> & Actions = {
    [ActionTypes.INC_COUNTER]({ commit }, payload: number) {
        commit(MutationTypes.INC_COUNTER, payload);
    },
    [ActionTypes.INC_NOTIFICATIONS]({ commit }, payload: object) {
        commit(MutationTypes.INC_NOTIFICATIONS, payload);
    },
    [ActionTypes.REMOVE_NOTIFICATIONS]({ commit }, id: string) {
        commit(MutationTypes.REMOVE_NOTIFICATIONS, id);
    },
    [ActionTypes.FETCH_FILTERS] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_FILTERS, payload);
    },
    [ActionTypes.FETCH_SETTINGS] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_SETTINGS, payload);
    },
    [ActionTypes.FETCH_SETTINGS_HELP] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_SETTINGS_HELP, payload);
    },
    [ActionTypes.FETCH_COUNTRY_STATE] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_COUNTRY_STATE, payload);
    },
    [ActionTypes.FETCH_CITY] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_CITY, payload);
    },
    [ActionTypes.FETCH_LOCALS] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_LOCALS, payload);
    },
    [ActionTypes.FETCH_STORES] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_STORES, payload);
    },
    [ActionTypes.FETCH_POSTS] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_POSTS, payload);
    },
    [ActionTypes.FETCH_SHARED_POST] ({ commit }, payload: string) {    
        commit(MutationTypes.SET_SHARED_POST, payload);
    },
    [ActionTypes.FETCH_STATIONS] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_STATIONS, payload);
    },
    [ActionTypes.FETCH_FEATURED] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_FEATURED, payload);
    },
    [ActionTypes.FETCH_AVATARS] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_AVATARS, payload);
    },
    [ActionTypes.FETCH_CATEGORIES] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_CATEGORIES, payload);
    },
    [ActionTypes.FETCH_USER] ({ commit }, payload: object) {    
        commit(MutationTypes.SET_USER, payload);
    }
};

// Getters types
export type Getters = {
    doubleCounter(state: State): number;
    settings(state: State): object;
    countryState(state: State): object;
    getUser(state: State): object;
    getAvatars(state: State): object;
};

//getters

export const getters: GetterTree<State, State> & Getters = {
    doubleCounter: state => {
        console.log("state", state.counter);
        return state.counter * 2;
    },
    settings: state => {
        console.log("state", state.countryState);
        return state.countryState;
    },
    countryState: state => {
        console.log("state", state.settings);
        return state.settings;
    },
    city: state => {
        console.log("state", state.city);
        return state.city;
    },
    locals: state => {
        console.log("state", state.locals);
        return state.locals;
    },
    stores: state => {
        console.log("state", state.stores);
        return state.stores;
    },
    posts: state => {
        console.log("state", state.posts);
        return state.posts;
    },
    sharedPost: state => {
        console.log("sharedPost", state.sharedPost);
        return state.sharedPost;
    },
    stations: state => {
        console.log("stations", state.stations);
        return state.stations;
    },
    featured: state => {
        console.log("featured", state.featured);
        return state.featured;
    },
    getUser: state => {
        console.log("state", state.user);
        return state.user;
    },
    getCategories: state => {
        console.log("state", state.categories);
        return state.categories;
    },
    getAvatars: state => {
        console.log("state", state.avatars);
        return state.avatars;
    },
    /* notifications */
    /* deleteNotification: (state) => {
        console.log("state", state.notifications);
        return state.counter * 2;
    }, */

};

//setup store type
export type Store = Omit<
    VuexStore<State>,
    "commit" | "getters" | "dispatch"
> & {
    commit<K extends keyof Mutations, P extends Parameters<Mutations[K]>[1]>(
        key: K,
        payload: P,
        options?: CommitOptions
    ): ReturnType<Mutations[K]>;
} & {
    getters: {
        [K in keyof Getters]: ReturnType<Getters[K]>;
    };
} & {
    dispatch<K extends keyof Actions>(
        key: K,
        payload: Parameters<Actions[K]>[1],
        options?: DispatchOptions
    ): ReturnType<Actions[K]>;
};

export const store = createStore({
    state,
    mutations,
    actions,
    getters,
    plugins: [createLogger()]
});

export function useStore() {
    return store as Store;
}
  // export const store = createStore({
  //   state,
  //   mutations: {
  //     increment(state, payload) {
  //       state.counter++;
  //     }
  //   },
  //   actions: {
  //     increment({ commit }) {
  //       commit("increment");
  //     }
  //   },

  //   getters: {
  //     counter(state) {
  //       return state.counter;
  //     }
  //   },
  //   modules: {}
  // });
